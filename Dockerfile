################################################################################
##                                 BASE IMAGE                                 ##
################################################################################

# Use node image from DockerHub as base
FROM node:13.12.0-slim AS base

# Set default work directory
WORKDIR /usr/src/api

# Set basic environment variables
ENV API_PORT=3000

# Expose default port
EXPOSE $API_PORT

################################################################################
##                              DEVELOPMENT IMAGE                             ##
################################################################################

# Create development image from base
FROM base AS development

# Add files that are necessary to intall dependencies
COPY package.json package-lock.json ./

# Install all dependencies
RUN npm install

# Add remaining code
COPY . .

# Set development environment variables
ENV NODE_ENV=development

# Run API in development mode
CMD ["npm", "run", "start:watch"]

################################################################################
##                                 TEST IMAGE                                 ##
################################################################################

# Create test image from base
FROM development AS test

# Set test environment variables
ENV NODE_ENV=test

# Run API in test mode
CMD ["npm", "run", "test:watch"]

################################################################################
##                                  CI IMAGE                                  ##
################################################################################

# Create ci image from base
FROM development AS ci

# Set test environment variables
ENV NODE_ENV=ci

# Run API in test mode
CMD ["npm", "run", "test"]

################################################################################
##                              PRODUCTION IMAGE                              ##
################################################################################

# Create production image from base
FROM development AS production

# Set production environment variables
ENV NODE_ENV=production \
    NO_UPDATE_NOTIFIER=true

# Remove all development dependencies
RUN npm prune --production

# Create non-root user to run API in production
RUN groupadd -g 999 api && \
    useradd -r -u 999 -g api api
USER api

# Run API in production mode
CMD ["npm", "run", "start"]
