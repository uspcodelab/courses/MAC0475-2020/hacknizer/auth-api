// External libraries
const request = require("supertest");
const jose = require("jose");

// API module
const API = require("../../src/api.js");

describe("When using fake secrets, the API", () => {
  const fakeSecrets = {
    keystore: new jose.JWKS.KeyStore([
      jose.JWK.generateSync("oct", 256, {
        alg: "HS256",
        kid: "symetric_cookie_sig",
        use: "sig",
      }),
    ]),
  };

  const mockedApi = API({ docs: {}, secrets: fakeSecrets });

  it("can sign cookies with a symetric key", async () => {
    const response = await request(mockedApi).get("/");

    const set_cookie_header = response.header["set-cookie"];
    const capture = set_cookie_header[1].match("(.*)=s%3A(.*)[.](.*);");
    const [_, cookie_name, cookie_value, cookie_signature] = capture;

    expect(cookie_name).toBe("ramanujan");
    expect(cookie_value).toBe("1729");
    expect(cookie_signature).toMatch(/[A-Za-z0-9%]+/);
  });
});
