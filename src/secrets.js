const fs = require("fs");
const jose = require("jose");

const API_SECRETS_PATH =
  process.env.API_SECRETS_PATH || "./.secrets/development";

const keystore = new jose.JWKS.KeyStore([
  jose.JWK.asKey(
    fs.readFileSync(`${API_SECRETS_PATH}/symetric_cookie_sig_key.pem`),
    {
      alg: "HS256",
      kid: "symetric_cookie_sig",
      use: "sig",
    }
  ),
]);

module.exports = { keystore };
