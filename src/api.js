const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");

module.exports = function ({ docs, secrets }) {
  const app = express();

  app.use(express.json());
  app.use(cors());
  app.use(cookieParser(secrets.keystore.get("symetric_cookie_sig").k));

  /**
   * @swagger
   *
   * /api-docs:
   *   get:
   *     description: OpenAPI documentation
   *     responses:
   *       200:
   *         description: Returns OpenAPI definition
   */
  app.get("/api-docs", (req, res) => {
    res.json(docs.swaggerSpec);
  });

  /**
   * @swagger
   *
   * /:
   *   get:
   *     description: Example path
   *     responses:
   *       200:
   *         description: Returns "Hello, World!"
   */
  app.get("/", (req, res) => {
    res.cookie("the_answer", "42");
    res.cookie("ramanujan", "1729", { signed: true });
    res.json("Hello, World!");
  });

  return app;
};
