// Internal modules
const docs = require("./docs.js");
const secrets = require("./secrets.js");

// API module
const API = require("./api.js");

const PORT = process.env.API_PORT || 3000;
const api = API({ docs, secrets });

api.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`));
