const swaggerJSDoc = require("swagger-jsdoc");

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Hello World",
      version: "0.0.1",
    },
  },
  apis: ["./src/api.js"],
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = { swaggerSpec };
